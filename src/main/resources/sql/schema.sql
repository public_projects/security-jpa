-- create database jwt_demo_1;;

use jwt_demo_1;

DROP TABLE IF EXISTS `txn_user_role`;
DROP TABLE IF EXISTS `ref_role`;
DROP TABLE IF EXISTS `txn_user`;

CREATE TABLE IF NOT EXISTS `txn_user`
(
    `userId`       binary(16)                              NOT NULL,
    `fullName`     varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `email`        varchar(100) COLLATE utf8mb4_unicode_ci          DEFAULT NULL,
    `mobileNo`     varchar(15) COLLATE utf8mb4_unicode_ci           DEFAULT NULL,
    `userName`     varchar(50) COLLATE utf8mb4_unicode_ci  NOT NULL,
    `pwd`          varchar(200) COLLATE utf8mb4_unicode_ci  NOT NULL,
    `fk_createdBy` binary(16)                                       DEFAULT NULL,
    `createdDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fk_updatedBy` binary(16)                                       DEFAULT NULL,
    `updatedDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `version`      int(11)                                          DEFAULT '0',
    `active`       bit(1)                                  NOT NULL DEFAULT b'1',
    `deleted`      bit(1)                                  NOT NULL DEFAULT b'0',
    PRIMARY KEY (`userId`),
    KEY `txn_user_fk1` (`fk_createdBy`),
    KEY `txn_user_fk2` (`fk_updatedBy`),
    KEY `userName` (`userName`),
    CONSTRAINT `txn_user_fk1` FOREIGN KEY (`fk_createdBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `txn_user_fk2` FOREIGN KEY (`fk_updatedBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO txn_user (
    userId,
    fullName,
    email,
    mobileNo,
    userName,
    pwd,
    fk_createdBy,
    createdDate,
    fk_updatedBy,
    updatedDate,
    version,
    active,
    deleted
) VALUES (
             UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')), -- Generate a random UUID and convert it to binary
             'Super User',
             'superuser@example.com',
             '123456789',
             'super_user',
             '$2a$12$rGO.bkwCSBw6ju3EY.LzxOc.52KONzcMxZZ3U0qUOvWFTNQA3O7Ce', -- hashed 123456 using https://bcrypt-generator.com/
             NULL,
             CURRENT_TIMESTAMP,
             NULL,
             CURRENT_TIMESTAMP,
             0,
             1,
             0
         );

CREATE TABLE IF NOT EXISTS `ref_role`
(
    `roleId`       binary(16)                              NOT NULL,
    `roleName`     varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `fk_createdBy` binary(16)                              DEFAULT NULL,
    `createdDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fk_updatedBy` binary(16)                              DEFAULT NULL,
    `updatedDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `version`      int(11)                                          DEFAULT '0',
    `active`       bit(1)                                  NOT NULL DEFAULT b'1',
    `deleted`      bit(1)                                  NOT NULL DEFAULT b'0',
    PRIMARY KEY (`roleId`),
    KEY `ref_role_fk1` (`fk_createdBy`),
    KEY `ref_role_fk2` (`fk_updatedBy`),
    KEY `roleName` (`roleName`),
    CONSTRAINT `ref_role_fk1` FOREIGN KEY (`fk_createdBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `ref_role_fk2` FOREIGN KEY (`fk_updatedBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO `ref_role` (`roleId`, `roleName`, `fk_createdBy`, `createdDate`, `fk_updatedBy`, `updatedDate`, `version`, `active`, `deleted`)
VALUES (UNHEX(REPLACE('d28e085e-84aa-11ee-b962-0242ac120002', '-', '')), 'ROLE_ADMIN', UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')), NOW(), UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')), NOW(), 0, 1, 0);

CREATE TABLE IF NOT EXISTS `txn_user_role`
(
    `userRoleId`   binary(16)                              NOT NULL,
    `fk_role`      binary(16)                              NOT NULL,
    `fk_user`      binary(16)                              NOT NULL,
    `fk_createdBy` binary(16)                                       DEFAULT NULL,
    `createdDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fk_updatedBy` binary(16)                                       DEFAULT NULL,
    `updatedDate`  datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `version`      int(11)                                          DEFAULT '0',
    `active`       bit(1)                                  NOT NULL DEFAULT b'1',
    `deleted`      bit(1)                                  NOT NULL DEFAULT b'0',
    PRIMARY KEY (`userRoleId`),
    KEY `txn_user_role_fk1` (`fk_createdBy`),
    KEY `txn_user_role_fk2` (`fk_updatedBy`),
    KEY `txn_user_role_fk3` (`fk_role`),
    CONSTRAINT `txn_user_role_fk1` FOREIGN KEY (`fk_createdBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `txn_user_role_fk2` FOREIGN KEY (`fk_updatedBy`) REFERENCES `txn_user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `txn_user_role_fk3` FOREIGN KEY (`fk_role`) REFERENCES `ref_role` (`roleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO `txn_user_role` (`userRoleId`, `fk_role`, `fk_user`, `fk_createdBy`, `createdDate`, `fk_updatedBy`, `updatedDate`, `version`, `active`, `deleted`)
VALUES (
           UNHEX(REPLACE(UUID(), '-', '')),
           UNHEX(REPLACE('d28e085e-84aa-11ee-b962-0242ac120002', '-', '')),
           UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')),
           UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')),
           NOW(),
           UNHEX(REPLACE('ab5b5554-8476-11ee-b962-0242ac120002', '-', '')),
           NOW(),
           0,
           1,
           0
       );