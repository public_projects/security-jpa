package org.green.jpa.security.repository.reference;

import org.green.jpa.security.entity.reference.RefRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RefRoleRepository extends JpaRepository<RefRole, UUID> {

    RefRole findByRoleName(String role);
}
