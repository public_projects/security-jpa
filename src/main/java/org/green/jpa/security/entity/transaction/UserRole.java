package org.green.jpa.security.entity.transaction;

import jakarta.persistence.*;
import org.green.jpa.security.entity.BaseEntity;
import org.green.jpa.security.entity.reference.RefRole;

import java.util.UUID;

@Entity
@Table(name = "txn_user_role")
public class UserRole extends BaseEntity {

    @Id
    @Column(name = "userRoleId", columnDefinition = "BINARY(16)", nullable = false)
    private UUID userRoleId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user")
    private UserEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_role")
    private RefRole refRole;

    public UUID getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(UUID userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public RefRole getRefRole() {
        return refRole;
    }

    public void setRefRole(RefRole refRole) {
        this.refRole = refRole;
    }
}
