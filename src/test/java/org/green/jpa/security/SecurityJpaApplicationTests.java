package org.green.jpa.security;

import org.green.jpa.security.repository.transaction.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SecurityJpaApplicationTests {

    private Logger logger = LoggerFactory.getLogger(SecurityJpaApplicationTests.class);
    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
        Assertions.assertEquals(2, userRepository.count());
    }

}
